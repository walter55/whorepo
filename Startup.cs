﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using System.Configuration;
using System.Diagnostics;
using Autofac;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Configuration;
//using Microsoft.AspNetCore.Http;

[assembly: OwinStartup("StartDashboard", typeof(HangfireDashboard.Startup))]

namespace HangfireDashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage(WebConfigurationManager.AppSettings["ConnectionStringHangfireDB"],
                new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                });
            app.UseHangfireDashboardCustomOptions(new HangfireDashboardCustomOptions
            {
                DashboardTitle = () => "Squirrel Job Dashboard (Version " +
                                                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + ")",
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter() }
            });
            app.UseHangfireDashboard("/hangfire1", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter(readOnlyDashboard:true) },
                IsReadOnlyFunc = (DashboardContext context) => true
            });
        }
    }
    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        private bool readOnlyDashboard;
        public MyAuthorizationFilter(bool readOnlyDashboard = false)
        {
            this.readOnlyDashboard = readOnlyDashboard;
        }
        public bool Authorize(DashboardContext context)
        {
            var ipList = GetIpList(this.readOnlyDashboard);
            return ipList.Contains(GetIPAddress());
        }
        protected string GetIPAddress()
        {
            var context = System.Web.HttpContext.Current;
            string ip = String.Empty;
            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            else if (!String.IsNullOrWhiteSpace(context.Request.UserHostAddress))
                ip = context.Request.UserHostAddress;
            return (ip=="::1")? "127.0.0.1":ip;
        }

        protected List<string> GetIpList(bool readOnlyDashboard)
        {
            if(readOnlyDashboard)
                return WebConfigurationManager.AppSettings["AllowedIPsReadOnly"].Split(',').ToList();
            else
                return WebConfigurationManager.AppSettings["AllowedIPs"].Split(',').ToList();
        }
    }
}